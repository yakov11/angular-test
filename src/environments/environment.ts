// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebaseConfig : {
    apiKey: "AIzaSyBZkNWi47PIa_xahdUPg3sW63vToM6-bP4",
    authDomain: "test-c6688.firebaseapp.com",
    projectId: "test-c6688",
    storageBucket: "test-c6688.appspot.com",
    messagingSenderId: "694723990945",
    appId: "1:694723990945:web:78b9dfbfce45662053e7e0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
